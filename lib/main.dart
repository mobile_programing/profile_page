import 'package:flutter/material.dart';

void main() {
  runApp(ContactProfilePage());
}
enum APP_THEME{LIHGT,DARK}

class MyAppTheme{
  static ThemeData appThemeLight() {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
        color:  Colors.white,
        iconTheme: IconThemeData(
        color: Colors.black,
    ),
    ),
    iconTheme: IconThemeData (
    color:  Colors.blue.shade900
    )
    );
  }

  static ThemeData appThemeDark(){
    return ThemeData(
    brightness: Brightness.dark,
      appBarTheme:  AppBarTheme(
        color: Colors.black,
        iconTheme: IconThemeData(
          color: Colors.white,
        )
      )
    );
  }
}

class ContactProfilePage extends StatefulWidget{
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIHGT;


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),

      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightGreenAccent,
          leading: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          actions: <Widget>[
            IconButton(onPressed: () {},
              icon: Icon(Icons.star_border),
              color: Colors.black,
            )
          ],

        ),

        body: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  width: double.infinity,

                  //Height constraint at Container widget level
                  height: 250,

                  child: Image.network(
                    "https://fbi.dek-d.com/27/0300/7319/118466494",
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                    height: 60,
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text("Yanika Kongpakdee",
                            style: TextStyle(fontSize: 30),
                          ),
                        )
                      ],
                    )
                ),
                Divider(
                  color: Colors.grey,
                ),

                Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: Theme(
                    data: ThemeData (
                        iconTheme: IconThemeData(
                          color: Colors.pink,
                        )
                    ),
                    child: profileActionItems(),
                  ),
                ),

                mobilePhoneListTile(),
                otherPhoneListTile(),
                Divider(
                  color: Colors.grey,
                ),

                emailListTile(),
                Divider(
                  color: Colors.grey,
                ),

                emailListTile(),
              ],
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
         child: Icon(Icons.threesixty),
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIHGT
                  : currentTheme = APP_THEME.DARK;
            });
          },

        ),
      ),
    );
  }}
Widget buildCallButton() {
  return Column(
      children: <Widget>[
      IconButton(
      icon: Icon(
      Icons.call,
      color: Colors.indigo.shade800,
  ),
  onPressed: () {},
  ),
  Text("Call"),
  ],
  );
}
Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}
Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}
Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}
Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("3300-803-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(""),
    title: Text("3300-803-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("63160193@go.buu.ac.th"),
    subtitle: Text("work"),

  );
}

Widget LocationlListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("No. 186, Village No. 4, Sub-district Kokyong, District NueaKlong,"
        " Province Krabi, Postal Code 81130 "),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget buildAppBarWidget() {
  return AppBar(
      backgroundColor: Colors.blue.shade200,
      leading: Icon(
        Icons.arrow_back,
        color: Colors.black,
      ),
      actions: <Widget>[
        IconButton(onPressed: () {},
          icon: Icon(Icons.star_border),
          color: Colors.black,
        )
      ],
  );
}

Widget profileActionItems(){
  return Row(
    mainAxisAlignment:  MainAxisAlignment.spaceEvenly,
    children: [
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}
Widget appBar(){
  return AppBar(
      backgroundColor: Colors.lightGreenAccent,
      leading: Icon(
        Icons.arrow_back,
        color: Colors.black,
      ),
      actions: <Widget>[
        IconButton(onPressed: () {},
          icon: Icon(Icons.star_border),
          color: Colors.black,
        )
      ],
  );
}

Widget body() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 250,

            child: Image.network(
              "https://fbi.dek-d.com/27/0300/7319/118466494",
              fit: BoxFit.cover,
            ),
          ),
          Container(
              height: 60,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text("Yanika Kongpakdee",
                      style: TextStyle(fontSize: 30),
                    ),
                  )
                ],
              )
          ),
          Divider(
            color: Colors.grey,
          ),

          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                  iconTheme: IconThemeData(
                    color: Colors.pink,
                  )
              ),
              child: profileActionItems(),
            ),
          ),

          mobilePhoneListTile(),
          otherPhoneListTile(),
          Divider(
            color: Colors.grey,
          ),

          emailListTile(),
          Divider(
            color: Colors.grey,
          ),

          emailListTile(),
        ],
      ),
    ],
  );
}


